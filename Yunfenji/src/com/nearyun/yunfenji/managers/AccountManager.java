package com.nearyun.yunfenji.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;

public class AccountManager {

	private static final String KEY_FILE_NAME = "account.io";
	private static final String KEY_NICKNAME = "nickname";
	private static final String KEY_ACCOUNT = "account";
	private static final String KEY_PASSWORD = "password";

	private static final String DEFAULT_NICKNAME = "";
	private static final String DEFAULT_ACCOUNT = "";
	private static final String DEFAULT_PASSWORD = "";

	private SharedPreferences spf;
	private Editor editor;

	public AccountManager(Context context) {
		spf = context.getSharedPreferences(KEY_FILE_NAME, Context.MODE_PRIVATE);
		editor = spf.edit();
	}

	public final String readAccount() {
		return readString(KEY_ACCOUNT, DEFAULT_ACCOUNT);
	}

	public final boolean saveAccount(String value) {
		return saveString(KEY_ACCOUNT, value);
	}

	public final String readNickname() {
		return readString(KEY_NICKNAME, DEFAULT_NICKNAME);
	}

	public final boolean saveNickname(String value) {
		return saveString(KEY_NICKNAME, value);
	}

	public final String readPassword() {
		return readString(KEY_PASSWORD, DEFAULT_PASSWORD);
	}

	public final boolean savePassword(String value) {
		return saveString(KEY_PASSWORD, value);
	}

	private final String readString(String key, String defValue) {
		if (TextUtils.isEmpty(key))
			return defValue;
		return spf.getString(key, defValue);
	}

	private final boolean saveString(String key, String value) {
		if (TextUtils.isEmpty(key))
			return false;
		editor.putString(key, value);
		return editor.commit();
	}
}
