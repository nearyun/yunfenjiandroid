package com.nearyun.yunfenji;

import android.app.Application;

import com.nearyun.sip.service.VoipService;
import com.tornado.util.XHLogUtil;

public class YFJApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		XHLogUtil.setLogDebug(this, true);
		VoipService.setSipTcpPort(5092);
	}

}
