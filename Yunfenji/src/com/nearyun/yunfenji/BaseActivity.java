package com.nearyun.yunfenji;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.tornado.util.NetworkUtil;

public abstract class BaseActivity extends Activity {
	protected String TAG = BaseActivity.class.getSimpleName();
	protected Activity mActivity;
	protected YFJApplication mApp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TAG = getClass().getSimpleName();

		mActivity = this;
		mApp = (YFJApplication) getApplication();

	}

	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	/**
	 * 显示toast信息
	 * 
	 * @param context
	 * @param msg
	 */
	public void showToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 判断当前网络是否可用
	 * 
	 * @return
	 */
	public boolean isNetworkAvailable() {
		if (NetworkUtil.IsNetWorkEnable(mActivity)) {
			return true;
		} else {
			showToast(getString(R.string.network_not_available));
			return false;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
