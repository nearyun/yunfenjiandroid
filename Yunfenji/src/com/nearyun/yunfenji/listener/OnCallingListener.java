package com.nearyun.yunfenji.listener;

import com.nearyun.yunfenji.models.Caller;

public interface OnCallingListener {

	public Caller getCaller();

	public void executeAnswer();

	public void exeuteReject();

	public void executeCallEnd();

	public void useSpeaker(boolean open);

	public void mute(boolean flag);

	public void keepalive();
}
