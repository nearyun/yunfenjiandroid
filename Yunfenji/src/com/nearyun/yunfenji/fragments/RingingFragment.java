package com.nearyun.yunfenji.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.nearyun.yunfenji.BaseFragment;
import com.nearyun.yunfenji.R;
import com.nearyun.yunfenji.listener.OnCallingListener;

public class RingingFragment extends BaseFragment implements View.OnClickListener {

	public static Fragment newInstance() {
		RingingFragment message = new RingingFragment();
		return message;
	}

	private ImageView avatarImageView;
	private TextView nickTextView;
	private ImageButton rejectButton, answerButton;

	private OnCallingListener mOnCallingListener;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof OnCallingListener) {
			mOnCallingListener = (OnCallingListener) activity;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (mRootView == null) {
			mRootView = inflater.inflate(R.layout.fragment_ringing, container, false);
			avatarImageView = (ImageView) mRootView.findViewById(R.id.avatarImageView);
			nickTextView = (TextView) mRootView.findViewById(R.id.nickNameTextView);
			rejectButton = (ImageButton) mRootView.findViewById(R.id.rejectButton);
			answerButton = (ImageButton) mRootView.findViewById(R.id.answerButton);

			rejectButton.setOnClickListener(this);
			answerButton.setOnClickListener(this);

			nickTextView.setText(mOnCallingListener.getCaller().getCallerName());
		}

		ViewGroup parent = (ViewGroup) mRootView.getParent();
		if (parent != null) {
			parent.removeView(mRootView);
		}
		return mRootView;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.rejectButton:
			executeReject();
			break;
		case R.id.answerButton:
			executeAnswer();
			break;
		default:
			break;
		}

	}

	private void executeAnswer() {
		mOnCallingListener.executeAnswer();
	}

	private void executeReject() {
		mOnCallingListener.exeuteReject();
	}

}
