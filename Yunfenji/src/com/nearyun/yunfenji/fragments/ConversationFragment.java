package com.nearyun.yunfenji.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.nearyun.yunfenji.BaseFragment;
import com.nearyun.yunfenji.R;
import com.nearyun.yunfenji.listener.OnCallingListener;
import com.nearyun.yunfenji.utils.LogUtil;
import com.tornado.util.TimeUtil;

public class ConversationFragment extends BaseFragment implements OnCheckedChangeListener, View.OnClickListener {

	public static Fragment newInstance() {
		ConversationFragment message = new ConversationFragment();
		return message;
	}

	private ImageView avatarImageView;
	private TextView nickNameTextView;
	private TextView durationTextView;
	private CheckBox speakerCheckBox;
	private CheckBox muteCheckBox;
	private ImageButton callEndButton;

	private long startTime = 0L;
	private OnCallingListener mOnCallingListener;

	private final Handler mHandler = new Handler();
	private boolean isRunning = false;

	private Runnable timerRunnable = new Runnable() {

		@Override
		public void run() {
			long diff = System.currentTimeMillis() - startTime;
			String timeString = TimeUtil.second2HMS((int) (diff / 1000), false);
			durationTextView.setText(timeString);
			mHandler.postDelayed(this, 1000);
		}
	};

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		startTime = System.currentTimeMillis();
		if (activity instanceof OnCallingListener) {
			mOnCallingListener = (OnCallingListener) activity;
		}
		isRunning = true;
		runKeepalive();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (mRootView == null) {
			mRootView = inflater.inflate(R.layout.fragment_conversation, container, false);

			avatarImageView = (ImageView) mRootView.findViewById(R.id.avatarImageView);
			nickNameTextView = (TextView) mRootView.findViewById(R.id.nickNameTextView);
			durationTextView = (TextView) mRootView.findViewById(R.id.durationTextView);
			speakerCheckBox = (CheckBox) mRootView.findViewById(R.id.speakerCheckBox);
			muteCheckBox = (CheckBox) mRootView.findViewById(R.id.muteCheckBox);
			callEndButton = (ImageButton) mRootView.findViewById(R.id.callEndButton);

			speakerCheckBox.setOnCheckedChangeListener(this);
			muteCheckBox.setOnCheckedChangeListener(this);
			callEndButton.setOnClickListener(this);

			nickNameTextView.setText(mOnCallingListener.getCaller().getCallerName());

			muteCheckBox.setChecked(false);
			speakerCheckBox.setChecked(false);
		}

		ViewGroup parent = (ViewGroup) mRootView.getParent();
		if (parent != null) {
			parent.removeView(mRootView);
		}
		return mRootView;
	}

	@Override
	public void onStart() {
		super.onStart();
		mHandler.post(timerRunnable);
	}

	@Override
	public void onStop() {
		super.onStop();
		mHandler.removeCallbacks(timerRunnable);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		isRunning = false;
	}

	@Override
	public void onClick(View v) {
		mOnCallingListener.executeCallEnd();
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()) {
		case R.id.speakerCheckBox:
			executeChangeSpeaker(isChecked);
			break;
		case R.id.muteCheckBox:
			exevuteChangeMute(isChecked);
			break;
		default:
			break;
		}

	}

	private void executeChangeSpeaker(boolean open) {
		mOnCallingListener.useSpeaker(open);
	}

	private void exevuteChangeMute(boolean open) {
		mOnCallingListener.mute(open);
	}

	private void runKeepalive() {
		new Thread() {
			public void run() {
				while (isRunning) {
					mOnCallingListener.keepalive();
					SystemClock.sleep(5000);
					LogUtil.d(TAG, "keepalive");
				}
			}
		}.start();
	}
}
