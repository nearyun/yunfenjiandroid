package com.nearyun.yunfenji.models;

/**
 * 通话纪录
 * 
 * @author xiaohua
 * 
 */
public class CallRecord {
	public static final int TYPE_PHONE_OUT = 0x100;
	public static final int TYPE_PHONE_IN = 0x101;
	public static final int TYPE_PHONE_MISS = 0x102;

	private String name;
	private String phone;
	private long time;
	private int type;// 呼入，呼出，未接

	public CallRecord(String _name, String _phone, long _time, int _type) {
		name = _name;
		phone = _phone;
		time = _time;
		type = _type;
	}

	public String getName() {
		return name;
	}

	public String getPhone() {
		return phone;
	}

	public long getTime() {
		return time;
	}

	public int getType() {
		return type;
	}
}
