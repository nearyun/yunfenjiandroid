package com.nearyun.yunfenji.models;

import java.io.Serializable;

public class Caller implements Serializable {
	private static final long serialVersionUID = 1953961659138493054L;
	
	private String callerUri;
	private String callerId;
	private String callerName;

	public Caller(String cUri, String cId, String cName) {
		this.callerUri = cUri;
		this.callerId = cId;
		this.callerName = cName;
	}

	public String getCallerUri() {
		return callerUri;
	}

	public String getCallerId() {
		return callerId;
	}

	public String getCallerName() {
		return callerName;
	}

}
