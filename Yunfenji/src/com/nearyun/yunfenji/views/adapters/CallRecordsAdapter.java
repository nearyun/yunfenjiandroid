package com.nearyun.yunfenji.views.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nearyun.yunfenji.R;
import com.nearyun.yunfenji.models.CallRecord;
import com.tornado.util.TimeUtil;
import com.tornado.util.ViewHolderUtil;

public class CallRecordsAdapter extends AbsBaseAdapter<CallRecord> {

	public CallRecordsAdapter(Context cxt, ArrayList<CallRecord> dataList) {
		super(cxt, dataList);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null)
			convertView = mInflater.inflate(R.layout.listitem_call_record, parent, false);
		CallRecord cr = mDataList.get(position);
		TextView nameTextView = ViewHolderUtil.get(convertView, R.id.item_name_textview);
		nameTextView.setText(cr.getName());
		TextView phoneTextView = ViewHolderUtil.get(convertView, R.id.item_phone_textview);
		phoneTextView.setText(cr.getPhone());
		final int type = cr.getType();
		if (type == CallRecord.TYPE_PHONE_IN) {
			phoneTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.phone_record_in_call, 0, 0, 0);
		} else if (type == CallRecord.TYPE_PHONE_MISS) {
			phoneTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.phone_record_miss_call, 0, 0, 0);
		} else if (type == CallRecord.TYPE_PHONE_OUT) {
			phoneTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.phone_record_out_call, 0, 0, 0);
		} else {
			phoneTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		}
		TextView timeTextView = ViewHolderUtil.get(convertView, R.id.item_time_textview);
		timeTextView.setText(TimeUtil.getMessageTime(cr.getTime()));

		return convertView;
	}

}
