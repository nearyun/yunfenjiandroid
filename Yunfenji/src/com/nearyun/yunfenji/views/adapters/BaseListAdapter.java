package com.nearyun.yunfenji.views.adapters;

import java.util.ArrayList;

import android.content.Context;

public abstract class BaseListAdapter<T> extends AbsBaseAdapter<T> {
	public BaseListAdapter(Context cxt) {
		super(cxt, new ArrayList<T>());
	}

	public void clear() {
		mDataList.clear();
	}

	public void addAllWithClear(ArrayList<T> list) {
		clear();
		addAll(list);
	}

	public void addAll(ArrayList<T> list) {
		mDataList.addAll(list);
	}
}
