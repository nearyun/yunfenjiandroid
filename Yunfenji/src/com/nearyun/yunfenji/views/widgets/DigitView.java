package com.nearyun.yunfenji.views.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nearyun.yunfenji.R;

public class DigitView extends LinearLayout {

	private TextView digitTextView;
	private TextView letterTextView;
	private String digit;

	public DigitView(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater.from(context).inflate(R.layout.widget_digit_view, this, true);

		digitTextView = (TextView) findViewById(R.id.digitTextView);
		letterTextView = (TextView) findViewById(R.id.letterTextView);
	}

	public String getDigit() {
		return digit;
	}

	public void setContent(String digit, String letter) {
		this.digit = digit;
		digitTextView.setText(digit);
		letterTextView.setText(letter);
	}

}
