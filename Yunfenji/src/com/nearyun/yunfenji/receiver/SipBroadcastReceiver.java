package com.nearyun.yunfenji.receiver;

import android.content.Context;

import com.nearyun.sip.receiver.AbsSipBroadcastReceiver;
import com.nearyun.yunfenji.activitys.OnCallingActivity;
import com.nearyun.yunfenji.models.Caller;

public class SipBroadcastReceiver extends AbsSipBroadcastReceiver {

	@Override
	protected void onSipMediaError(Context context, int index) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onSipRedirected(Context context, int index, String target) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onSipProceeding(Context context, int index, int proceedingStatus) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onSipIdle(Context context, int index) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onSipFailure(Context context, int index, int failureStatus, String reason) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onSipDisconnected(Context context, int index) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onSipConnected(Context context, int index) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onSipAnswer(Context context, int index) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onSipInvite(Context context, int index, String callerUri, String callerId, String callerName) {
		if (OnCallingActivity.isRunning())
			return;
		OnCallingActivity.newInstanceFromBroadcast(context, new Caller(callerUri, callerId, callerName));
	}

	@Override
	protected void onSipUnregistered(Context context) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onSipRegistered(Context context) {
		// TODO Auto-generated method stub

	}
}
