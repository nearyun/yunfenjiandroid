package com.nearyun.yunfenji.utils;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Vibrator;

import com.nearyun.yunfenji.R;

public class RingToneMedia {

	private MediaPlayer mPlayer = null;

	private boolean isVibrating = false;

	private Context mContext;

	public RingToneMedia(Context context) {
		mContext = context;
	}

	public void playIncomingTone() {
		stopTone();
		mPlayer = MediaPlayer.create(mContext, R.raw.voip_incoming);
		if (mPlayer != null) {
			mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mPlayer.setLooping(true);
			mPlayer.start();
		}
		Vibrator vibrator = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(new long[] { 0, 1000, 800 }, 0);
		isVibrating = true;
	}

	public void playCloseTone() {
		stopTone();
		mPlayer = MediaPlayer.create(mContext, R.raw.voip_close);
		if (mPlayer != null) {
			mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mPlayer.setLooping(true);
			mPlayer.start();
		}
	}

	public void playConnectingTone() {
		stopTone();
		mPlayer = MediaPlayer.create(mContext, R.raw.voip_connecting);
		if (mPlayer != null) {
			mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mPlayer.setLooping(true);
			mPlayer.start();
		}
	}

	public void playNoResponseTone() {
		stopTone();
		mPlayer = MediaPlayer.create(mContext, R.raw.voip_no_response);
		if (mPlayer != null) {
			mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mPlayer.setLooping(true);
			mPlayer.start();
		}
	}

	public void playPeerBusyTone() {
		stopTone();
		mPlayer = MediaPlayer.create(mContext, R.raw.voip_peer_busy);
		if (mPlayer != null) {
			mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mPlayer.setLooping(true);
			mPlayer.start();
		}
	}

	public void playBeerRejectTone() {
		stopTone();
		mPlayer = MediaPlayer.create(mContext, R.raw.voip_peer_reject);
		if (mPlayer != null) {
			mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mPlayer.setLooping(true);
			mPlayer.start();
		}
	}

	public void stopTone() {
		if (mPlayer != null && mPlayer.isPlaying()) {
			mPlayer.stop();
			mPlayer.release();
			mPlayer = null;
		}
		if (isVibrating) {
			Vibrator vibrator = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
			vibrator.cancel();
			isVibrating = false;
		}
	}
}
