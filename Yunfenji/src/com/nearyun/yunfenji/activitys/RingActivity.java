//package com.nearyun.yunfenji.activitys;
//
//import android.content.Context;
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.ImageButton;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.nearyun.sip.model.SipPhoneService;
//import com.nearyun.sip.model.SipPhoneService.OnServiceConnectionListener;
//import com.nearyun.yunfenji.R;
//import com.nearyun.yunfenji.notification.NotificationUtil;
//import com.nearyun.yunfenji.utils.RingToneMedia;
//import com.tornado.util.ScreenManager;
//
///**
// * 振铃界面
// * 
// * @author xiaohua
// * 
// */
//public class RingActivity extends BaseSipActivity implements OnClickListener, OnServiceConnectionListener {
//
//	private static final String KEY_CALLER_NAME = "KEY_CALLER_NAME";
//	private static final String KEY_CALLER_ID = "KEY_CALLER_ID";
//	private static final String KEY_CALLER_URI = "KEY_CALLER_URI";
//
//	public static final void newInstance(Context context, String callerUri, String callerId, String callerName) {
//		Intent intent = new Intent(context, RingActivity.class);
//		intent.putExtra(KEY_CALLER_URI, callerUri);
//		intent.putExtra(KEY_CALLER_ID, callerId);
//		intent.putExtra(KEY_CALLER_NAME, callerName);
//		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//		context.startActivity(intent);
//	}
//
//	private ImageView avatarImageView;
//	private TextView nickTextView;
//	private ImageButton rejectButton, answerButton;
//
//	private String callerUri;
//	private String callerId;
//	private String callerName;
//	private SipPhoneService mSipPhoneService;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_ring);
//
//		Intent intent = getIntent();
//		if (intent == null) {
//			finish();
//			return;
//		}
//
//		avatarImageView = (ImageView) findViewById(R.id.avatarImageView);
//		nickTextView = (TextView) findViewById(R.id.nickNameTextView);
//		rejectButton = (ImageButton) findViewById(R.id.rejectButton);
//		answerButton = (ImageButton) findViewById(R.id.answerButton);
//
//		rejectButton.setOnClickListener(this);
//		answerButton.setOnClickListener(this);
//
//		disableOperationButton();
//
//		mSipPhoneService = new SipPhoneService(this);
//		mSipPhoneService.setOnServiceConnectionListener(this);
//		mSipPhoneService.doBindService();
//
//		callerUri = intent.getStringExtra(KEY_CALLER_URI);
//		callerId = intent.getStringExtra(KEY_CALLER_ID);
//		callerName = intent.getStringExtra(KEY_CALLER_NAME);
//		nickTextView.setText(callerUri);
//
//	}
//
//	@Override
//	protected void onPostCreate(Bundle savedInstanceState) {
//		super.onPostCreate(savedInstanceState);
//		ScreenManager.setUnlocked(mActivity);
//	}
//
//	@Override
//	protected void onRestart() {
//		super.onRestart();
//		dismissNotification();
//	}
//
//	@Override
//	protected void onStop() {
//		super.onStop();
//		showNotification(callerName);
//	}
//
//	@Override
//	protected void onDestroy() {
//		super.onDestroy();
//		mSipPhoneService.doUnbindService();
//		ScreenManager.setLocked(mActivity);
//		dismissNotification();
//		// RingToneMedia.stopTone(mActivity);
//	}
//
//	private void showNotification(String callerName) {
//		String ticker = callerName + " 来电……";
//		String title = ticker;
//		NotificationUtil.showNotification(mActivity, RingActivity.class, ticker, title, "点击返回");
//	}
//
//	private void dismissNotification() {
//		NotificationUtil.dismissNotification(mActivity);
//	}
//
//	@Override
//	public void onClick(View v) {
//		switch (v.getId()) {
//		case R.id.rejectButton:
//			executeReject();
//			break;
//		case R.id.answerButton:
//			executeAnswer();
//			break;
//		default:
//			break;
//		}
//
//	}
//
//	private void disableOperationButton() {
//		rejectButton.setEnabled(false);
//		answerButton.setEnabled(false);
//	}
//
//	private void enableOperationButton() {
//		rejectButton.setEnabled(true);
//		answerButton.setEnabled(true);
//	}
//
//	@Override
//	public void onBackPressed() {
//		// do nothing
//	}
//
//	private void executeAnswer() {
//		mSipPhoneService.answer();
//		disableOperationButton();
//	}
//
//	private void executeReject() {
//		mSipPhoneService.reject(603);
//		disableOperationButton();
//	}
//
//	@Override
//	protected void onSipIdle(int index) {
//		// RingToneMedia.stopTone(mActivity);
//		finish();
//	}
//
//	@Override
//	protected void onSipConnected(int index) {
//		// RingToneMedia.stopTone(mActivity);
//		ConversationActivity.newInstance(mActivity, callerUri);
//		finish();
//	}
//
//	@Override
//	protected void onSipFailure(int index, int failureStatus, String reason) {
//		disableOperationButton();
//		// RingToneMedia.stopTone(mActivity);
//		finish();
//	}
//
//	@Override
//	public void onConnected() {
//		// RingToneMedia.playTone(mActivity);
//		enableOperationButton();
//	}
//
//	@Override
//	public void onDisconnected() {
//
//	}
//}
