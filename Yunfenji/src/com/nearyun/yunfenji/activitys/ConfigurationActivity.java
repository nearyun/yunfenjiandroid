package com.nearyun.yunfenji.activitys;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.nearyun.sip.model.SipAccount;
import com.nearyun.sip.model.SipPhoneService;
import com.nearyun.yunfenji.BaseActivity;
import com.nearyun.yunfenji.R;
import com.nearyun.yunfenji.constants.Contants;
import com.nearyun.yunfenji.listener.OnTextChangedListener;
import com.nearyun.yunfenji.managers.AccountManager;
import com.tornado.util.DeviceUtil;

/**
 * 账号配置界面
 * 
 * @author xiaohua
 * 
 */
public class ConfigurationActivity extends BaseActivity {

	public static final void newInstance(Context context) {
		Intent intent = new Intent(context, ConfigurationActivity.class);
		context.startActivity(intent);
	}

	private EditText nicknameEditText, accountEditText, passwordEditText;
	private Button saveButton;
	private AccountManager mAccountManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_configuration);

		nicknameEditText = (EditText) findViewById(R.id.nicknameEditText);
		accountEditText = (EditText) findViewById(R.id.accountEditText);
		passwordEditText = (EditText) findViewById(R.id.passwordEditText);
		saveButton = (Button) findViewById(R.id.saveButton);

		mAccountManager = new AccountManager(mActivity);
		nicknameEditText.setText(mAccountManager.readNickname());
		nicknameEditText.setSelection(nicknameEditText.getText().length());
		accountEditText.setText(mAccountManager.readAccount());
		passwordEditText.setText(mAccountManager.readPassword());

		nicknameEditText.addTextChangedListener(mOnTextChangedListener);
		accountEditText.addTextChangedListener(mOnTextChangedListener);
		passwordEditText.addTextChangedListener(mOnTextChangedListener);

		saveButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				executeSave();
				saveButton.setEnabled(false);
			}
		});
	}

	protected void executeSave() {
		String nickName = nicknameEditText.getText().toString();
		if (TextUtils.isEmpty(nickName)) {
			showToast("昵称不可为空");
			return;
		}
		mAccountManager.saveNickname(nickName);

		String account = accountEditText.getText().toString();
		if (TextUtils.isEmpty(account)) {
			showToast("账号不可为空");
			return;
		}
		mAccountManager.saveAccount(account);

		String password = passwordEditText.getText().toString();
		if (TextUtils.isEmpty(password)) {
			showToast("密码不可为空");
			return;
		}
		mAccountManager.savePassword(password);

		SipAccount sipAccount = new SipAccount(nickName, account, Contants.DOMAIN, Contants.PROXY_SERVER, account,
				password, DeviceUtil.getDeviceUUID(mActivity), Contants.ICE_SERVER);
		SipPhoneService mSipPhoneService = new SipPhoneService(mActivity);
		mSipPhoneService.doStopService();
		mSipPhoneService.doStartService(sipAccount);
		finish();
	}

	private OnTextChangedListener mOnTextChangedListener = new OnTextChangedListener() {

		@Override
		public void afterTextChanged(Editable s) {
			boolean flag = (nicknameEditText.length() > 0 && accountEditText.length() > 0 && passwordEditText.length() > 0);
			saveButton.setEnabled(flag);
		}
	};

	protected void onSipRegistered() {
		showToast("注册成功");
		finish();
	}

	protected void onSipUnregistered() {
		showToast("注册失败");
	}
}
