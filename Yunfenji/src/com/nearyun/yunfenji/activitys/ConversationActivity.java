//package com.nearyun.yunfenji.activitys;
//
//import android.content.Context;
//import android.content.Intent;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.SystemClock;
//import android.view.View;
//import android.widget.CheckBox;
//import android.widget.CompoundButton;
//import android.widget.CompoundButton.OnCheckedChangeListener;
//import android.widget.ImageButton;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.nearyun.sip.model.SipPhoneService;
//import com.nearyun.sip.model.SipPhoneService.OnServiceConnectionListener;
//import com.nearyun.yunfenji.R;
//import com.nearyun.yunfenji.utils.LogUtil;
//import com.tornado.util.TimeUtil;
//
///**
// * 会话界面
// * 
// * @author xiaohua
// * 
// */
//public class ConversationActivity extends BaseSipActivity implements OnCheckedChangeListener, View.OnClickListener,
//		OnServiceConnectionListener {
//
//	private static final String KEY_CALLER_URI = "KEY_CALLER_URI";
//
//	public static final void newInstance(Context context, String callerUri) {
//		Intent intent = new Intent(context, ConversationActivity.class);
//		intent.putExtra(KEY_CALLER_URI, callerUri);
//		context.startActivity(intent);
//	}
//
//	private static boolean isRunning = false;
//
//	public static boolean isRunning() {
//		return isRunning;
//	}
//
//	private ImageView avatarImageView;
//	private TextView nickNameTextView;
//	private TextView durationTextView;
//	private CheckBox speakerCheckBox;
//	private CheckBox muteCheckBox;
//	private ImageButton callEndButton;
//
//	private final Handler mHandler = new Handler();
//
//	private Runnable timerRunnable = new Runnable() {
//
//		@Override
//		public void run() {
//			long diff = System.currentTimeMillis() - startTime;
//			String timeString = TimeUtil.second2HMS((int) (diff / 1000), false);
//			durationTextView.setText(timeString);
//			mHandler.postDelayed(this, 1000);
//		}
//	};
//
//	private long startTime = 0L;
//	private String callerUri;
//	private SipPhoneService mSipPhoneService;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_conversation);
//		isRunning = true;
//
//		avatarImageView = (ImageView) findViewById(R.id.avatarImageView);
//		nickNameTextView = (TextView) findViewById(R.id.nickNameTextView);
//		durationTextView = (TextView) findViewById(R.id.durationTextView);
//		speakerCheckBox = (CheckBox) findViewById(R.id.speakerCheckBox);
//		muteCheckBox = (CheckBox) findViewById(R.id.muteCheckBox);
//		callEndButton = (ImageButton) findViewById(R.id.callEndButton);
//
//		speakerCheckBox.setOnCheckedChangeListener(this);
//		muteCheckBox.setOnCheckedChangeListener(this);
//		callEndButton.setOnClickListener(this);
//
//		mSipPhoneService = new SipPhoneService(this);
//		mSipPhoneService.setOnServiceConnectionListener(this);
//		mSipPhoneService.doBindService();
//
//		startTime = System.currentTimeMillis();
//
//		Intent intent = getIntent();
//		if (intent == null) {
//			finish();
//			return;
//		}
//
//		callerUri = intent.getStringExtra(KEY_CALLER_URI);
//		nickNameTextView.setText(callerUri);
//	}
//
//	@Override
//	protected void onPostCreate(Bundle savedInstanceState) {
//		super.onPostCreate(savedInstanceState);
//		mHandler.post(timerRunnable);
//	}
//
//	@Override
//	protected void onDestroy() {
//		super.onDestroy();
//		isRunning = false;
//		mSipPhoneService.doUnbindService();
//		mHandler.removeCallbacks(timerRunnable);
//	}
//
//	@Override
//	public void onClick(View v) {
//		executeCallEnd();
//	}
//
//	private void executeCallEnd() {
//		mSipPhoneService.hangup();
//	}
//
//	@Override
//	public void onBackPressed() {
//		// do nothing
//	}
//
//	@Override
//	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//		switch (buttonView.getId()) {
//		case R.id.speakerCheckBox:
//			executeChangeSpeaker(isChecked);
//			break;
//		case R.id.muteCheckBox:
//			exevuteChangeMute(isChecked);
//			break;
//		default:
//			break;
//		}
//
//	}
//
//	private void executeChangeSpeaker(boolean open) {
//		mSipPhoneService.useSpeaker(open);
//	}
//
//	private void exevuteChangeMute(boolean open) {
//		mSipPhoneService.mute(open);
//	}
//
//	private void runKeepalive() {
//		new Thread() {
//			public void run() {
//				while (isRunning) {
//					mSipPhoneService.keepalive();
//					SystemClock.sleep(5000);
//					LogUtil.d(TAG, "keepalive");
//				}
//			}
//		}.start();
//	}
//
//	@Override
//	public void onConnected() {
//		mSipPhoneService.register();
//		mSipPhoneService.mute(false);
//		runKeepalive();
//		muteCheckBox.setChecked(mSipPhoneService.isMute());
//		speakerCheckBox.setChecked(mSipPhoneService.isUseSpeaker());
//	}
//
//	@Override
//	public void onDisconnected() {
//
//	}
//
//	@Override
//	protected void onSipFailure(int index, int failureStatus, String reason) {
//		showToast("对方暂时无法接听，请稍后再拨");
//		finish();
//	}
//
//	@Override
//	protected void onSipDisconnected(int index) {
//		finish();
//	}
//
//	@Override
//	protected void onSipInvite(int index, String callerUri, String callerId, String callerName) {
//		// TODO Auto-generated method stub
//
//	}
//}
