package com.nearyun.yunfenji.activitys;

import java.io.Serializable;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.nearyun.sip.SipClient;
import com.nearyun.sip.model.SipPhoneService;
import com.nearyun.sip.model.SipPhoneService.OnServiceConnectionListener;
import com.nearyun.yunfenji.R;
import com.nearyun.yunfenji.fragments.ConversationFragment;
import com.nearyun.yunfenji.fragments.RingingFragment;
import com.nearyun.yunfenji.listener.OnCallingListener;
import com.nearyun.yunfenji.models.Caller;
import com.nearyun.yunfenji.notification.NotificationUtil;
import com.nearyun.yunfenji.utils.LogUtil;
import com.nearyun.yunfenji.utils.RingToneMedia;
import com.tornado.util.ScreenManager;

public class OnCallingActivity extends BaseSipActivity implements OnCallingListener, OnServiceConnectionListener {

	/** 呼叫 */
	private static final String ACTION_CALL_SOMEONE = "com.nearyun.yunfenji.activitys.ACTION_CALL_SOMEONE";
	/** 被呼 */
	private static final String ACTION_SOMEONE_INVITE = "com.nearyun.yunfenji.activitys.ACTION_SOMEONE_INVITE";

	public static void newInstanceFromDial(Context context, Caller caller) {
		Intent intent = new Intent(context, OnCallingActivity.class);
		intent.setAction(ACTION_CALL_SOMEONE);
		intent.putExtra(KEY_CALLER, caller);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}

	private static final String KEY_CALLER = "KEY_CALLER";

	public static final void newInstanceFromBroadcast(Context context, Caller caller) {
		Intent intent = new Intent(context, OnCallingActivity.class);
		intent.setAction(ACTION_SOMEONE_INVITE);
		intent.putExtra(KEY_CALLER, caller);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(intent);
	}

	private static final int STEP_RINGING = 0x100;
	private static final int STEP_CONVERSATION = 0x101;

	private int currentStep = STEP_RINGING;
	private Caller mCaller;
	private SipPhoneService mSipPhoneService = null;
	private RingToneMedia mRingToneMedia;

	private static boolean isRunning = false;

	private final Handler mHandler = new Handler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_on_calling);

		isRunning = true;

		mSipPhoneService = new SipPhoneService(mActivity);
		mSipPhoneService.setOnServiceConnectionListener(this);
		mSipPhoneService.doBindService();

		Intent intent = getIntent();
		if (intent == null) {
			finish();
			return;
		}

		Serializable tmp = intent.getSerializableExtra(KEY_CALLER);
		if (tmp == null || !(tmp instanceof Caller)) {
			finish();
			return;
		} else {
			mCaller = (Caller) tmp;
		}

		if (intent.getAction().equals(ACTION_CALL_SOMEONE)) {
			changeToConversationFragment();
		} else if (intent.getAction().equals(ACTION_SOMEONE_INVITE)) {
			changeToRingingFragment();
		} else {
			finish();
			return;
		}

		mRingToneMedia = new RingToneMedia(mActivity);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		ScreenManager.setUnlocked(mActivity);
		if (currentStep == STEP_RINGING) {
			mRingToneMedia.playIncomingTone();
		}
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		dismissNotification();
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (STEP_RINGING == currentStep)
			showNotification(mCaller.getCallerName() + " 来电……");
		else
			showNotification(mCaller.getCallerName() + " 通话……");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		isRunning = false;

		mHandler.removeCallbacks(finishRunnable);

		ScreenManager.setLocked(mActivity);
		mRingToneMedia.stopTone();
		dismissNotification();
		mSipPhoneService.doUnbindService();
	}

	private void showNotification(String ticker) {
		String title = ticker;
		NotificationUtil.showNotification(mActivity, OnCallingActivity.class, ticker, title, "点击返回");
	}

	private void dismissNotification() {
		NotificationUtil.dismissNotification(mActivity);
	}

	private void changeToRingingFragment() {
		currentStep = STEP_RINGING;
		FragmentManager fragmentManager = getFragmentManager();
		Fragment fragment = RingingFragment.newInstance();
		fragmentManager.beginTransaction().replace(R.id.root_oncalling_layout, fragment).commit();
	}

	private void changeToConversationFragment() {
		currentStep = STEP_CONVERSATION;
		FragmentManager fragmentManager = getFragmentManager();
		Fragment fragment = ConversationFragment.newInstance();
		fragmentManager.beginTransaction().replace(R.id.root_oncalling_layout, fragment).commit();
	}

	@Override
	public void onBackPressed() {
		// do nothing
	}

	@Override
	protected void onSipProceeding(int index, int proceedingStatus) {
		if (STEP_CONVERSATION == currentStep) {
			mRingToneMedia.playConnectingTone();
		}
	}

	@Override
	protected void onSipFailure(int index, int failureStatus, String reason) {
		if (STEP_RINGING == currentStep) {
			finish();
			return;
		} else if (STEP_CONVERSATION == currentStep) {
			if (failureStatus == SipClient.CODE_REJECT_603) {
				mRingToneMedia.playBeerRejectTone();
			} else if (failureStatus == SipClient.CODE_REJECT_480) {
				mRingToneMedia.playPeerBusyTone();
			} else if (failureStatus == SipClient.CODE_REJECT_487) {
				mRingToneMedia.playNoResponseTone();
			} else {
				mRingToneMedia.playCloseTone();
			}
		}
		mHandler.postDelayed(finishRunnable, 5000);
	}

	private Runnable finishRunnable = new Runnable() {

		@Override
		public void run() {
			finish();
		}

	};

	@Override
	protected void onSipDisconnected(int index) {
		if (STEP_CONVERSATION == currentStep) {
			mRingToneMedia.playCloseTone();
		}
		mHandler.postDelayed(finishRunnable, 5000);
	}

	@Override
	protected void onSipConnected(int index) {
		mRingToneMedia.stopTone();
		if (STEP_RINGING == currentStep) {
			changeToConversationFragment();
		}
	}

	@Override
	protected void onSipInvite(int index, String callerUri, String callerId, String callerName) {
		mSipPhoneService.callReject(index, SipClient.CODE_REJECT_480);
	}

	@Override
	public Caller getCaller() {
		return mCaller;
	}

	@Override
	public void executeAnswer() {
		mSipPhoneService.answer();
	}

	@Override
	public void exeuteReject() {
		mSipPhoneService.reject(SipClient.CODE_REJECT_603);
		finish();
	}

	@Override
	public void executeCallEnd() {
		mSipPhoneService.hangup();
		finish();
	}

	public static boolean isRunning() {
		return isRunning;
	}

	@Override
	public void keepalive() {
		mSipPhoneService.keepalive();
	}

	@Override
	public void useSpeaker(boolean open) {
		mSipPhoneService.useSpeaker(open);
	}

	@Override
	public void mute(boolean flag) {
		mSipPhoneService.mute(flag);
	}

	@Override
	public void onConnected() {
		LogUtil.i(TAG, "service is onConnected");
		mute(false);
		useSpeaker(currentStep == STEP_RINGING);
	}

	@Override
	public void onDisconnected() {
		LogUtil.w(TAG, "service is onDisconnected");
	}

}
