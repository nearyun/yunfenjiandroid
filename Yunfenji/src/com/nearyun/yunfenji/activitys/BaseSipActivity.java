package com.nearyun.yunfenji.activitys;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Bundle;

import com.nearyun.sip.model.SipPhoneIntentFilter;
import com.nearyun.sip.receiver.AbsSipBroadcastReceiver;
import com.nearyun.yunfenji.BaseActivity;

/**
 * sip Activity基类
 * 
 * @author xiaohua
 * 
 */
public abstract class BaseSipActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		registerReceiver(sipBroadcastReceiver, SipPhoneIntentFilter.getFilter(mActivity));
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(sipBroadcastReceiver);
	}

	private final BroadcastReceiver sipBroadcastReceiver = new AbsSipBroadcastReceiver() {

		@Override
		protected void onSipMediaError(Context context, int index) {
			BaseSipActivity.this.onSipMediaError(index);
		}

		@Override
		protected void onSipRedirected(Context context, int index, String target) {
			BaseSipActivity.this.onSipRedirected(index, target);
		}

		@Override
		protected void onSipProceeding(Context context, int index, int proceedingStatus) {
			BaseSipActivity.this.onSipProceeding(index, proceedingStatus);
		}

		@Override
		protected void onSipIdle(Context context, int index) {
			BaseSipActivity.this.onSipIdle(index);
		}

		@Override
		protected void onSipFailure(Context context, int index, int failureStatus, String reason) {
			BaseSipActivity.this.onSipFailure(index, failureStatus, reason);
		}

		@Override
		protected void onSipDisconnected(Context context, int index) {
			BaseSipActivity.this.onSipDisconnected(index);
		}

		@Override
		protected void onSipConnected(Context context, int index) {
			BaseSipActivity.this.onSipConnected(index);
		}

		@Override
		protected void onSipAnswer(Context context, int index) {
			BaseSipActivity.this.onSipAnswer(index);
		}

		@Override
		protected void onSipInvite(Context context, int index, String callerUri, String callerId, String callerName) {
			BaseSipActivity.this.onSipInvite(index, callerUri, callerId, callerName);
		}

		@Override
		protected void onSipUnregistered(Context context) {
			BaseSipActivity.this.onSipUnregistered();
		}

		@Override
		protected void onSipRegistered(Context context) {
			BaseSipActivity.this.onSipRegistered();
		}

	};

	protected void onSipRegistered() {
		// TODO Auto-generated method stub

	}

	protected void onSipMediaError(int index) {
		// TODO Auto-generated method stub

	}

	protected void onSipRedirected(int index, String target) {
		// TODO Auto-generated method stub

	}

	protected void onSipProceeding(int index, int proceedingStatus) {
		// TODO Auto-generated method stub

	}

	protected void onSipIdle(int index) {
		// TODO Auto-generated method stub

	}

	protected void onSipFailure(int index, int failureStatus, String reason) {
		// TODO Auto-generated method stub

	}

	protected void onSipDisconnected(int index) {
		// TODO Auto-generated method stub

	}

	protected void onSipConnected(int index) {
		// TODO Auto-generated method stub

	}

	protected void onSipAnswer(int index) {
		// TODO Auto-generated method stub

	}

	protected void onSipInvite(int index, String callerUri, String callerId, String callerName) {
		// TODO Auto-generated method stub

	}

	protected void onSipUnregistered() {
		// TODO Auto-generated method stub

	}
}
