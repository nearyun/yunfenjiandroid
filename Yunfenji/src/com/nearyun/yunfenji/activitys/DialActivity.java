package com.nearyun.yunfenji.activitys;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.nearyun.sip.model.SipAccount;
import com.nearyun.sip.model.SipPhoneService;
import com.nearyun.yunfenji.R;
import com.nearyun.yunfenji.constants.Contants;
import com.nearyun.yunfenji.listener.OnTextChangedListener;
import com.nearyun.yunfenji.managers.AccountManager;
import com.nearyun.yunfenji.models.CallRecord;
import com.nearyun.yunfenji.models.Caller;
import com.nearyun.yunfenji.views.adapters.CallRecordsAdapter;
import com.nearyun.yunfenji.views.widgets.DigitView;
import com.tornado.util.DeviceUtil;

/**
 * 拨号界面
 * 
 * @author xiaohua
 * 
 */
public class DialActivity extends BaseSipActivity implements View.OnClickListener {

	private TextView sipAccountTextView;
	private TextView sipStatusTextView;
	private ImageButton configureImagebutton;
	private CallRecordsAdapter mCallRecordsAdapter;
	private ListView phoneContactsSuggestionListview;
	private final ArrayList<CallRecord> callRecords = new ArrayList<CallRecord>();
	private CheckBox dialpadCheckbox;
	private ImageButton callButton;
	private ImageButton backspaceImagebutton;
	private EditText phoneDigitsEditText;
	private View phoneDialpadLayout;

	private SipPhoneService mSipPhoneService = null;
	private AccountManager mAccountManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dial);

		initView();

		mAccountManager = new AccountManager(mActivity);
		mSipPhoneService = new SipPhoneService(mActivity);
		String nickName = mAccountManager.readNickname();
		String account = mAccountManager.readAccount();
		String password = mAccountManager.readPassword();
		if (!TextUtils.isEmpty(account) && !TextUtils.isEmpty(password)) {
			SipAccount sipAccount = new SipAccount(nickName, account, Contants.DOMAIN, Contants.PROXY_SERVER, account,
					password, DeviceUtil.getDeviceUUID(mActivity), Contants.ICE_SERVER);
			mSipPhoneService.doStartService(sipAccount);
			sipAccountTextView.setText(mAccountManager.readAccount());
			sipStatusTextView.setText("正在注册 ");
		} else {
			sipAccountTextView.setText("账号未配置");
			sipStatusTextView.setText("");
		}
		mSipPhoneService.doBindService();
	}

	private void initView() {
		sipAccountTextView = (TextView) findViewById(R.id.sip_account_textview);
		sipStatusTextView = (TextView) findViewById(R.id.sip_status_textview);

		configureImagebutton = (ImageButton) findViewById(R.id.configure_imagebutton);
		configureImagebutton.setOnClickListener(this);

		phoneContactsSuggestionListview = (ListView) findViewById(R.id.phone_contacts_suggestion_listview);
		phoneContactsSuggestionListview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				CallRecord cr = (CallRecord) mCallRecordsAdapter.getItem(position);
				phoneDigitsEditText.setText(cr.getPhone());
				dialpadCheckbox.setChecked(true);
			}
		});
		mCallRecordsAdapter = new CallRecordsAdapter(mActivity, callRecords);
		callRecords.add(new CallRecord("小米", "472208860", System.currentTimeMillis(), CallRecord.TYPE_PHONE_OUT));
		callRecords.add(new CallRecord("魅族", "472208861", System.currentTimeMillis(), CallRecord.TYPE_PHONE_IN));
		callRecords.add(new CallRecord("努比亚", "472208862", System.currentTimeMillis(), CallRecord.TYPE_PHONE_MISS));
		phoneContactsSuggestionListview.setAdapter(mCallRecordsAdapter);

		dialpadCheckbox = (CheckBox) findViewById(R.id.dialpad_checkbox);
		dialpadCheckbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					phoneDialpadLayout.setVisibility(View.VISIBLE);
				else
					phoneDialpadLayout.setVisibility(View.INVISIBLE);
				changeCtrlView();
			}

		});

		callButton = (ImageButton) findViewById(R.id.call_button);
		callButton.setOnClickListener(this);

		backspaceImagebutton = (ImageButton) findViewById(R.id.backspace_imagebutton);
		backspaceImagebutton.setOnClickListener(this);
		backspaceImagebutton.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				phoneDigitsEditText.setText("");
				return false;
			}
		});

		phoneDigitsEditText = (EditText) findViewById(R.id.phone_digits_edit_text);
		phoneDigitsEditText.addTextChangedListener(new OnTextChangedListener() {

			@Override
			public void afterTextChanged(Editable s) {
				changeCtrlView();
			}
		});

		phoneDialpadLayout = findViewById(R.id.phone_dialpad_layout);

		DigitView one = (DigitView) findViewById(R.id.oneDigitView);
		one.setContent("1", "");
		one.setOnClickListener(this);
		DigitView two = (DigitView) findViewById(R.id.twoDigitView);
		two.setContent("2", "ABC");
		two.setOnClickListener(this);
		DigitView three = (DigitView) findViewById(R.id.threeDigitView);
		three.setContent("3", "DEF");
		three.setOnClickListener(this);

		DigitView four = (DigitView) findViewById(R.id.fourDigitView);
		four.setContent("4", "GHI");
		four.setOnClickListener(this);
		DigitView five = (DigitView) findViewById(R.id.fiveDigitView);
		five.setContent("5", "JKL");
		five.setOnClickListener(this);
		DigitView six = (DigitView) findViewById(R.id.sixDigitView);
		six.setContent("6", "MNO");
		six.setOnClickListener(this);

		DigitView seven = (DigitView) findViewById(R.id.sevenDigitView);
		seven.setContent("7", "PQRS");
		seven.setOnClickListener(this);
		DigitView eight = (DigitView) findViewById(R.id.eightDigitView);
		eight.setContent("8", "TUV");
		eight.setOnClickListener(this);
		DigitView nine = (DigitView) findViewById(R.id.nineDigitView);
		nine.setContent("9", "WXYZ");
		nine.setOnClickListener(this);

		DigitView star = (DigitView) findViewById(R.id.starDigitView);
		star.setContent("*", "");
		star.setOnClickListener(this);
		DigitView zero = (DigitView) findViewById(R.id.zeroDigitView);
		zero.setContent("0", "+");
		zero.setOnClickListener(this);
		DigitView sharp = (DigitView) findViewById(R.id.sharpDigitView);
		sharp.setContent("#", "");
		sharp.setOnClickListener(this);

		dialpadCheckbox.setChecked(true);
		changeCtrlView();
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mSipPhoneService.doUnbindService();
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addCategory(Intent.CATEGORY_HOME);
		startActivity(intent);
	}

	@Override
	public void onClick(View v) {
		final int id = v.getId();
		if (id == R.id.configure_imagebutton) {
			openConfigurationActivity();
		} else if (id == R.id.call_button) {
			String phone = phoneDigitsEditText.getText().toString();
			executeCall(phone);
		} else if (id == R.id.backspace_imagebutton) {
			executeDelete();
		} else if (v instanceof DigitView) {
			DigitView view = (DigitView) v;
			updateEditText(view.getDigit());
		}
	}

	private void changeCtrlView() {
		if (!dialpadCheckbox.isChecked()) {
			callButton.setVisibility(View.INVISIBLE);
			backspaceImagebutton.setVisibility(View.INVISIBLE);
			return;
		}
		if (phoneDigitsEditText.length() > 0) {
			callButton.setVisibility(View.VISIBLE);
			backspaceImagebutton.setVisibility(View.VISIBLE);
		} else {
			callButton.setVisibility(View.INVISIBLE);
			backspaceImagebutton.setVisibility(View.INVISIBLE);
		}
	}

	private void updateEditText(String digit) {
		String content = phoneDigitsEditText.getText().toString() + digit;
		phoneDigitsEditText.setText(content);
	}

	private void openConfigurationActivity() {
		ConfigurationActivity.newInstance(mActivity);
	}

	private void executeDelete() {
		String content = phoneDigitsEditText.getText().toString();
		if (TextUtils.isEmpty(content))
			return;
		int length = content.length();
		content = content.substring(0, length - 1);
		phoneDigitsEditText.setText(content);
	}

	private void executeCall(String phone) {
		String uri = "sip:" + phone + "@192.168.1.119";
		mSipPhoneService.invite(uri);
		// ConversationActivity.newInstance(mActivity, phone);
		OnCallingActivity.newInstanceFromDial(mActivity, new Caller(uri, "", phone));
	}

	protected void onSipRegistered() {
		sipAccountTextView.setText(mAccountManager.readAccount());
		sipStatusTextView.setText("已注册");
	}

	protected void onSipIdle(int index) {
		sipAccountTextView.setText(mAccountManager.readAccount());
		sipStatusTextView.setText("准备就绪");
	}

	protected void onSipUnregistered() {
		sipAccountTextView.setText(mAccountManager.readAccount());
		sipStatusTextView.setText("已注销");
	}
}
