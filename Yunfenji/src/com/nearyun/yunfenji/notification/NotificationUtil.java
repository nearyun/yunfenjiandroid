package com.nearyun.yunfenji.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.nearyun.yunfenji.R;

public class NotificationUtil {
	public static final int SIP_NOTIFICATION_FLAG = 1;

	// public static final void showNotification(Context context, String text) {
	// showNotification(context, MainActivity.class, SIP_NOTIFICATION_FLAG, "",
	// "会享正在运行中", text);
	// }
//
//	public static final void showHuaweiNotification(Context context, Intent intent, String ticker, String title,
//			String text) {
//		NotificationManager manger = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//		Notification notification = new Notification.Builder(context).setSmallIcon(R.drawable.ic_launcher)
//				.setTicker(ticker).setContentTitle(title).setContentText(text).setContentIntent(pendingIntent)
//				.getNotification();
//		notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.incoming);
//		// notification.defaults = Notification.DEFAULT_SOUND;
//		notification.flags |= Notification.FLAG_AUTO_CANCEL;
//		manger.notify(1, notification);
//	}

	public static final void dismissNotification(Context context) {
		dismissNotification(context, SIP_NOTIFICATION_FLAG);
	}

	public static final <T> void showNotification(Context context, Class<T> activityClass, String ticker, String title,
			String text) {
		showNotification(context, activityClass, SIP_NOTIFICATION_FLAG, ticker, title, text);
	}

	@SuppressWarnings("deprecation")
	private static final <T> void showNotification(Context context, Class<T> activityClass, int notificationId,
			String ticker, String title, String text) {
		NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context, activityClass), 0);
		Notification onMeetingNotification = new Notification.Builder(context).setSmallIcon(R.drawable.ic_launcher)
				.setTicker(ticker).setContentTitle(title).setContentText(text).setContentIntent(pendingIntent)
				.getNotification();
		onMeetingNotification.flags = Notification.FLAG_ONGOING_EVENT;
		onMeetingNotification.flags |= Notification.FLAG_NO_CLEAR;
		manager.notify(notificationId, onMeetingNotification);
	}

	private static final void dismissNotification(Context context, int notificationId) {
		NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		manager.cancel(notificationId);
	}
}
