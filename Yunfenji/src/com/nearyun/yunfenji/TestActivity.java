package com.nearyun.yunfenji;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.nearyun.yunfenji.views.widgets.DigitView;

public class TestActivity extends BaseActivity implements View.OnClickListener {

	public static final void newInstance(Context context) {
		Intent intent = new Intent(context, TestActivity.class);
		context.startActivity(intent);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialpad);

		DigitView one = (DigitView) findViewById(R.id.oneDigitView);
		one.setContent("1", "");
		one.setOnClickListener(this);
		DigitView two = (DigitView) findViewById(R.id.twoDigitView);
		two.setContent("2", "ABC");
		two.setOnClickListener(this);
		DigitView three = (DigitView) findViewById(R.id.threeDigitView);
		three.setContent("3", "DEF");
		three.setOnClickListener(this);

		DigitView four = (DigitView) findViewById(R.id.fourDigitView);
		four.setContent("4", "GHI");
		four.setOnClickListener(this);
		DigitView five = (DigitView) findViewById(R.id.fiveDigitView);
		five.setContent("5", "JKL");
		five.setOnClickListener(this);
		DigitView six = (DigitView) findViewById(R.id.sixDigitView);
		six.setContent("6", "MNO");
		six.setOnClickListener(this);

		DigitView seven = (DigitView) findViewById(R.id.sevenDigitView);
		seven.setContent("7", "PQRS");
		seven.setOnClickListener(this);
		DigitView eight = (DigitView) findViewById(R.id.eightDigitView);
		eight.setContent("8", "TUV");
		eight.setOnClickListener(this);
		DigitView nine = (DigitView) findViewById(R.id.nineDigitView);
		nine.setContent("9", "WXYZ");
		nine.setOnClickListener(this);

		DigitView star = (DigitView) findViewById(R.id.starDigitView);
		star.setContent("*", "");
		star.setOnClickListener(this);
		DigitView zero = (DigitView) findViewById(R.id.zeroDigitView);
		zero.setContent("0", "+");
		zero.setOnClickListener(this);
		DigitView sharp = (DigitView) findViewById(R.id.sharpDigitView);
		sharp.setContent("#", "");
		sharp.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		System.out.println(v.toString());
		if (v instanceof DigitView) {
			DigitView view = (DigitView) v;
			showToast(view.getDigit());
		}
	}
}
