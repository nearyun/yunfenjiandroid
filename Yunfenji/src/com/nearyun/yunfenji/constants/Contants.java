package com.nearyun.yunfenji.constants;

public class Contants {

	private static final boolean BETA = false;

	public static final String DOMAIN = BETA ? "192.168.1.119" : "micyun.com";
	public static final String PROXY_SERVER = BETA ? "sip:192.168.1.119:5090;transport=TCP;lr"
			: "sip:120.25.216.119:5090;transport=TCP;lr";
	public static final String ICE_SERVER = "stun.micyun.com";

}
